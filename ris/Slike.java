package ris;

public class Slike {
	private String potDoNaslovneSlike;
	private String[] potDoSlikeDogodka;
	
	Slike(String potDoNaslovneSlike){
		this.potDoNaslovneSlike = potDoNaslovneSlike;
	}
	
	public String getPotDoNaslovneSlike() {
		return potDoNaslovneSlike;
	}
	
	public String[] getPotDoSlikeDogodka() {
		return potDoSlikeDogodka;
	}
	
	public void setPotDoNaslovneSlike(String potDoNaslovneSlike) {
		this.potDoNaslovneSlike = potDoNaslovneSlike;
	}
	
	public void setPotDoSlikeDogodka(String[] potDoSlikeDogodka) {
		this.potDoSlikeDogodka = potDoSlikeDogodka;
	}
}
