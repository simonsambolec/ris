package ris;
public class Uporabnik {

	private String ime;
	private String priimek;
	private String e_naslov;
	private String geslo;
	private String telefonska_st;
	private String naslov;
	
	public Uporabnik(String ime, String priimek, String e_naslov, String geslo) {
		this.ime = ime;
		this.priimek = priimek;
		this.e_naslov = e_naslov;
		this.geslo = geslo;
	}
	
	public boolean preveriEnaslovInGeslo(String e_naslov, String geslo){
		if(this.e_naslov.equals(e_naslov) && this.geslo.equals(geslo))return true;
		return false;
	}
	
	/**
	 * 
	 * @param Vstopnica
	 */
	public void nakupVstopnice(int Vstopnica) {
		// TODO - implement Uporabnik.nakupVstopnice
		throw new UnsupportedOperationException();
	}

	public String getNaslov() {
		return this.naslov;
	}

	/**
	 * 
	 * @param naslov
	 */
	public void setNaslov(String naslov) {
		this.naslov = naslov;
	}

	public String getTelefonska_st() {
		return this.telefonska_st;
	}

	
	
	/**
	 * 
	 * @param telefonska_st
	 */
	public void setTelefonska_st(String telefonska_st) {
		this.telefonska_st = telefonska_st;
	}

	public String getGeslo() {
		return geslo;
	}

	/**
	 * 
	 * @param geslo
	 */
	public void SetGeslo(String geslo) {
		this.geslo = geslo;
	}

	public String getE_naslov() {
		return this.e_naslov;
	}

	/**
	 * 
	 * @param e_naslov
	 */
	public void setE_naslov(String e_naslov) {
		this.e_naslov = e_naslov;
	}

	public String getPriimek() {
		return this.priimek;
	}

	/**
	 * 
	 * @param priimek
	 */
	public void setPriimek(String priimek) {
		this.priimek = priimek;
	}

	public String getIme() {
		return this.ime;
	}

	/**
	 * 
	 * @param ime
	 */
	public void setIme(String ime) {
		this.ime = ime;
	}

}