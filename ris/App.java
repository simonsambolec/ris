
package ris;

import java.io.FileWriter;
import java.io.IOException;

import java.time.LocalDate;
import java.util.Scanner;
import java.util.*;

public class App {

	public static void nakupVstopnice(int idUporabnika, int idDogodka, int idVstopnice) {

		try {
			FileWriter myWriter = new FileWriter("narocila.txt", true);
			myWriter.write(
					LocalDate.now().toString() + ": " + idUporabnika + ", " + idDogodka + ", " + idVstopnice + "\n");
			myWriter.close();
			System.out.println("Successfully wrote to the file.");
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}

		System.out.println("Nakup zabele�en.");
	}

	public static void prikaziDogodke(ArrayList<Dogodek> dogodki) {
		System.out.println("Dogodki:");
		Dogodek dogodek;
		for (int i = 0; i < dogodki.size(); i++) {
			dogodek = dogodki.get(i);
			System.out.println((i + 1) + ". ----------------------------------------------------------");
			dogodek.prikaziDogodek();
		}
	}

	public static LocalDate stringToLocalDate(String datum) {
		String[] arrOfStr = datum.split("\\.");
		return LocalDate.of(Integer.parseInt(arrOfStr[2]), Integer.parseInt(arrOfStr[1]),
				Integer.parseInt(arrOfStr[0]));
	}

	public static Dogodek dodajDogodek() {
		Scanner in = new Scanner(System.in);
		Dogodek dogodek = new Dogodek();

		System.out.println("Vnesi naziv dogodka: ");
		String niz = in.nextLine();
		dogodek.setNaziv(niz);
		System.out.println("Vnesi datum dogodka (dan.mesec.leto): ");
		niz = in.nextLine();
		dogodek.setDatum_od(stringToLocalDate(niz));
		System.out.println("Vnesi datum do (oziroma 0, �e je 1 datum): ");
		niz = in.nextLine();
		if (niz.equals("0")) {

		} else {
			dogodek.setDatum_do(stringToLocalDate(niz));
		}
		System.out.println("Vnesi uro dogodka: ");
		niz = in.nextLine();
		dogodek.setUra_dogodka(niz);

		System.out.println("Vnesi stevilo vstopnic: ");
		String nizst = in.nextLine();
		int st = Integer.parseInt(nizst);
		if (st <= 0) {

		} else {
			Vstopnica[] vstopnice = new Vstopnica[st];
			String naziv;
			String cena;
			String steviloKart;
			for (int i = 0; i < st; i++) {
				System.out.println("Vnesi naziv " + (i + 1) + ". vstopnice: ");
				naziv = in.nextLine();

				System.out.println("Vnesi ceno " + (i + 1) + ". vstopnice: ");
				cena = in.nextLine();

				System.out.println("Vnesi kolicino " + (i + 1) + ". vstopnice: ");
				steviloKart = in.nextLine();

				vstopnice[i] = new Vstopnica(naziv, Float.parseFloat(cena), Integer.parseInt(steviloKart));
			}
			dogodek.setVstopnice(vstopnice);
		}

		return dogodek;
	}

	public static void main(String[] args) {
		ArrayList<Uporabnik> uporabniki = new ArrayList<Uporabnik>();
		uporabniki.add(new Uporabnik("Simon", "Sambolec", "s@s.si", "1234"));
		uporabniki.add(new Uporabnik("Nik", "Volgemut", "nik@v.com", "5678"));
		uporabniki.add(new Uporabnik("Nal", "Gucek", "nal@s.si", "abcd123"));

		Scanner in = new Scanner(System.in);

		Dogodek dd = new Dogodek("Fa�enk 2021", LocalDate.now(), "20:00");
		dd.setDatum_do(LocalDate.of(2021, 2, 8));

		Dogodek ad = new Dogodek("Pozdrav poletju", LocalDate.now(), "21:00");

		ArrayList<Dogodek> dogodki = new ArrayList<Dogodek>();
		dogodki.add(dd);
		dogodki.add(ad);

		
		int indexUser = 0;
		String e_naslov;
		String geslo;
		boolean prijavljen = false;
		while(prijavljen == false) {
			System.out.println("PRIJAVA: ");
			System.out.println("Vnesi e-naslov: ");
			e_naslov = in.nextLine();
			System.out.println("Vnesi geslo: ");
			geslo = in.nextLine();
			
			for(int i = 0; i < uporabniki.size(); i++) {
				if(uporabniki.get(i).preveriEnaslovInGeslo(e_naslov, geslo) == true) {
					prijavljen = true;
					indexUser = i;
					break;
				}
			}
		}
		System.out.println("Pozdravljen " + uporabniki.get(indexUser).getIme() + " " + uporabniki.get(indexUser).getPriimek() + "!");
		
		Dogodek b = dodajDogodek();
		dogodki.add(b);
		
		int a = 1;
		String input;
		while (a != 0) {
			prikaziDogodke(dogodki);
			System.out.print("Vnesi stevilko dogodka: ");
			input = in.nextLine();
			a = Integer.parseInt(input);
			dogodki.get(a - 1).prikaziPodrobnostiDogodka();
			System.out.println("Vnesi stevilko vstopnice, ki jo �eli� kupiti (oziroma 0, da se vrne�): ");
			input = in.nextLine();
			if (input.equals("0")) {

			} else {
				nakupVstopnice(0, a, (Integer.parseInt(input) - 1));
				dogodki.get(a - 1).odstraniEnoVstopnico(Integer.parseInt(input) - 1);
			}

		}

		System.out.println("Nasvidenje!");
		in.close();
	}
}
