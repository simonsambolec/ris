package ris;

import java.time.LocalDate;

public class Narocilo {

	private LocalDate datumNarocila;
	private int idUporabnika;
	private int idDogodka;
	private int idVstopnice;

	public Narocilo() {
		// TODO Auto-generated constructor stub
	}
	
	public LocalDate getDatumNarocila() {
		return this.datumNarocila;
	}

	/**
	 * 
	 * @param datumNarocila
	 */
	public void setDatumNarocila(LocalDate datumNarocila) {
		this.datumNarocila = datumNarocila;
	}
	 
	public void setIdUporabnika(int idUporabnika) {
		this.idUporabnika = idUporabnika;
	}
	
	public void setIdVstopnice(int idVstopnice) {
		this.idVstopnice = idVstopnice;
	}
	
	public void setIdDogodka(int idDogodka) {
		this.idDogodka = idDogodka;
	}
	

}