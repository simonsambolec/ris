package ris;
public class Vstopnica {

	private String naziv;
	private float cena;
	private int stevilo;
	
	public Vstopnica() {
		// TODO Auto-generated constructor stub
	}
	
	public Vstopnica(String naziv, float cena) {
		this.naziv = naziv;
		this.cena = cena;
	}
	
	public Vstopnica(String naziv, float cena, int stevilo) {
		this.naziv = naziv;
		this.cena = cena;
		this.stevilo = stevilo;
	}
	
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	
	public String getNaziv() {
		return naziv;
	}
	
	

	public float getCena() {
		return this.cena;
	}

	/**
	 * 
	 * @param cena
	 */
	public void setCena(float cena) {
		this.cena = cena;
	}
	
	public int getStevilo() {
		return stevilo;
	}
	
	public void setStevilo(int stevilo) {
		this.stevilo = stevilo;
	}
	
	public void odstraniEnoVstopnico() {
		if(this.stevilo == 0) {
			
		}else {
			this.stevilo = this.stevilo - 1;
		}
	}
}