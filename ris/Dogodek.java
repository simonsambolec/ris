package ris;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Dogodek {

	private String naziv;
	private LocalDate datum_od;
	private LocalDate datum_do;
	private Vstopnica[] vstopnice;
	private String ura_dogodka;
	private Slike slike;

	public Dogodek() {

	}

	public Dogodek(String naziv, LocalDate datum_od, String ura_dogodka) {
		this.naziv = naziv;
		this.datum_od = datum_od;
		this.ura_dogodka = ura_dogodka;
	}

	public void prikaziDogodek() {
		System.out.print(this.naziv + ", " + this.datum_od.format(DateTimeFormatter.ofPattern("dd. MMMM yyyy")));
		if (this.datum_do != null)
			System.out.print(" - " + this.datum_do.format(DateTimeFormatter.ofPattern("dd. MMMM yyyy")));
		System.out.print(" " + this.ura_dogodka);
		System.out.println();
	}

	public void prikaziPodrobnostiDogodka() {
		System.out.println("---------PODROBNOSTI---------");
		System.out.println("Naziv: " + this.naziv);
		System.out.print("Datum: " + this.datum_od.format(DateTimeFormatter.ofPattern("dd. MMMM yyyy")));
		if (this.datum_do != null)
			System.out.print(" - " + this.datum_do.format(DateTimeFormatter.ofPattern("dd. MMMM yyyy")));
		System.out.println();
		System.out.println("Ura dogodka: " + this.ura_dogodka);
		System.out.println("Vstopnice:");
		boolean vstopniceSoRazprodane = true;
		if (this.vstopnice != null) {
			for (int i = 0; i < vstopnice.length; i++) {
				if (vstopnice[i].getStevilo() == 0) {

				} else {
					vstopniceSoRazprodane = false;
					System.out.println(
							(i + 1) + ". " + this.vstopnice[i].getNaziv() + " " + this.vstopnice[i].getCena() + " � (Stevilo vstopnic na voljo: " + this.vstopnice[i].getStevilo() + ")");
				}
			}
		}
		if (vstopniceSoRazprodane == true)
			System.out.println("Za ta dogodek ni vstopnic na voljo.");

	}

	public String getNaziv() {
		return this.naziv;
	}

	/**
	 * 
	 * @param naziv
	 */
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public LocalDate getDatum_od() {
		return this.datum_od;
	}

	/**
	 * 
	 * @param datum_od
	 */
	public void setDatum_od(LocalDate datum_od) {
		this.datum_od = datum_od;
	}

	public LocalDate getDatum_do() {
		return this.datum_do;
	}

	/**
	 * 
	 * @param datum_do
	 */
	public void setDatum_do(LocalDate datum_do) {
		this.datum_do = datum_do;
	}

	public Vstopnica[] getVstopnice() {
		return this.vstopnice;
	}

	public Vstopnica getVstopnica(int index) {
		return this.vstopnice[index];
	}

	public void odstraniEnoVstopnico(int index) {
		this.getVstopnica(index).odstraniEnoVstopnico();
	}

	/**
	 * 
	 * @param vstopnice
	 */
	public void setVstopnice(Vstopnica[] vstopnice) {
		this.vstopnice = vstopnice;
	}

	public Slike getSlike() {
		return slike;
	}

	public void setSlike(Slike slike) {
		this.slike = slike;
	}

	public void setUra_dogodka(String ura_dogodka) {
		this.ura_dogodka = ura_dogodka;
	}

}